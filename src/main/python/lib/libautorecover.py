import os
import signal
import subprocess
import sys
import syslog

def log(message, priority=syslog.LOG_INFO):
    """
    Logs a message to syslog and prints it on stdout/stderr
    
    The log is opened immediately before logging the message, and closed immediately after.
    Each log entry is prepended with the identifier 'autorecover'.
    Log messages are additionally printed on the console: priority syslog.LOG_ERR and higher are
    printed on stderr, lower priorities on stdout.
    
    The following conventions for priorities apply:
    syslog.LOG_ERR: Conditions which prevent autorecover from running altogether, such as an
        invalid configuration.
    syslog.LOG_WARNING: Messages about failed tests; messages about recovery actions (including
        skipped ones); conditions which prevent individual module instances from running, e.g.
        because a dependency returned an error.
    syslog.LOG_NOTICE: Output from test backends or restore commands that might indicate why an
        operation failed
    
    message: The log message. If the message contains newlines, each line will be logged separately.
    priority: One of syslog.LOG_EMERG, syslog.LOG_ALERT, syslog.LOG_CRIT, syslog.LOG_ERR, syslog.LOG_WARNING, syslog.LOG_NOTICE, syslog.LOG_INFO, syslog.LOG_DEBUG
    """
    syslog.openlog('autorecover')
    for line in message.splitlines():
        syslog.syslog(priority, line)
        # FIXME: Priority levels seem to be 0 (LOG_EMERG) to 7 (LOG_DEBUG), higher bits can be used to
        # indicate log facilities and log options. Down below is not a pretty way to filter for priority;
        # it works for now but might break if things change under the hood.
        if (priority & 7) <= syslog.LOG_ERR:
            print(line, file=sys.stderr)
        else:
            print(line)
    syslog.closelog

# From https://stackoverflow.com/questions/42471475/fastest-way-to-get-system-uptime-in-python-in-linux.
# This implementation should be portable to any platform which has `uptime`, whereas others
# rely on platform-specific features such as `/proc/uptime` (which works on Linux but not on BSD).
def uptime():
    """
    Returns system uptime (retrieved by running `uptime`)
    """
    raw = subprocess.check_output('uptime').decode("utf8").replace(',', '')
    # We need to correctly process all forms `raw` can take, including:
    # 12:20  up 20 mins, 1 user, load averages: 0.67, 0.60, 0.48
    # 11:52  up 23:59, 2 users, load averages: 0.64, 0.59, 0.56
    # 11:53  up 1 day, 0 sec, 2 users, load averages: 0.49, 0.55, 0.54
    # 11:54  up 1 day, 1 min, 2 users, load averages: 0.40
    # 11:55  up 1 day, 2 mins, 2 users, load averages: 0.56, 0.52, 0.53
    # 11:52  up 1 day, 23:59, 2 users, load averages: 0.64, 0.59, 0.56
    # 11:53  up 2 days, 0 sec, 2 users, load averages: 0.49, 0.55, 0.54
    # 11:54  up 2 days, 1 min, 2 users, load averages: 0.40, 0.51, 0.53
    # 11:52  up 2 days, 23:59, 2 users, load averages: 0.64, 0.59, 0.56
    if 'day' in raw:
        i_hhmmss = 4
        days = int(raw.split()[2])
    else:
        i_hhmmss = 2
        days = 0
    if 'sec' in raw:
        hours = 0
        minutes = 0
        seconds = int(raw.split()[i_hhmmss])
    elif 'min' in raw:
        hours = 0
        minutes = int(raw.split()[i_hhmmss])
        seconds = 0
    else:
        hours, minutes = map(int,raw.split()[i_hhmmss].split(':'))
        seconds = 0
    totalsecs = ((days * 24 + hours) * 60 + minutes) * 60 + seconds
    return totalsecs

def killtree(pid):
    """
    Kills a process and all its descendants
    
    Invoking `Popen.kill()` or `os.kill()` with `signal.SIGKILL` will kill a single process, but
    any children of the process will be adopted by PID 1. This function will kill an entire tree of
    processes of any depth, ensuring child processes get killed before their parent.
    
    Descendants of a process are determined by running `pgrep -P PID`, which works on Linux and
    FreeBSD, possibly also on other Unix-like OSes.
    
    pid: The PID of the process to kill
    """
    args = ['pgrep', '-P', str(pid)]
    try:
        for child_pid in subprocess.check_output(args).decode("utf8").splitlines():
            killtree(child_pid)
    except subprocess.CalledProcessError as e:
        if e.returncode != 1:
            log("Unable to enumerate child processes for {0} (pgrep exited with status {1}), child processes will not get killed".format(pid, e.returncode), syslog.LOG_WARNING)
    os.kill(int(pid), signal.SIGKILL)
